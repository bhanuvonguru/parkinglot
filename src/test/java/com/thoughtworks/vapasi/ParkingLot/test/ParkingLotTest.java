package com.thoughtworks.vapasi.ParkingLot.test;

import com.thoughtworks.vapasi.ParkingLot.ParkingLot;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import static org.junit.jupiter.api.Assertions.*;

class ParkingLotTest {
    ParkingLot parking = new ParkingLot();


    @Test
    void shouldBeAbleToParkTheVehicle() {
        assertEquals(String.class,parking.parkVehicle(new Object()).getClass());
    }

    @Test
    void shouldNotBeAbleToParkWhenThereIsNoVehicle() {
        assertThrows(IllegalArgumentException.class, ()->parking.parkVehicle(null));
    }

    @Test
    void shouldBeAbleToUnparkAVehicle(){
        assertNotNull(parking.UnparkVehicle("someId"));
    }
}