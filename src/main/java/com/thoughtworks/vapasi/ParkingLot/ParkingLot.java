package com.thoughtworks.vapasi.ParkingLot;
import java.util.HashMap;
import java.util.UUID;
public class ParkingLot {
    private HashMap<String,Object> parkingIdToCarMap= new HashMap<>();

    public ParkingLot(){

    }

    public String parkVehicle(Object car){
        if(car == null){
            throw new IllegalArgumentException();
        }
        String uniqueId=generateUniqueId();
        parkingIdToCarMap.put(uniqueId,car);
        return uniqueId;
    }
    public Object UnparkVehicle(String uniqueId){
        return new Object();
    }

    String generateUniqueId(){
        return UUID.randomUUID().toString();

    }

}
